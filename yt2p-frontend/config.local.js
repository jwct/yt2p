module.exports = {
  AXIOS_BASE_URL : "https://m0q6svfak7.execute-api.ap-east-1.amazonaws.com/dev",
  COGNITO_ENDPOINTS: {
    authorization: "https://yt2p.auth.ap-southeast-1.amazoncognito.com/login",
    token: "https://yt2p.auth.ap-southeast-1.amazoncognito.com/oauth2/token",
    userInfo: "https://yt2p.auth.ap-southeast-1.amazoncognito.com/oauth2/userInfo",
    logout: "https://yt2p.auth.ap-southeast-1.amazoncognito.com/logout"
  } , 
  AUTH: {
    redirectUri: "http://localhost:3000/login",
    logoutRedirectUri: "http://localhost:3000/login",
    clientId: "1dn48c586ihm34asceic24jn17",
  },
  ROUTER: {
    base: '',
  }
};
