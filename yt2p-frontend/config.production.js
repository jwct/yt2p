module.exports = {
  AXIOS_BASE_URL : "https://m0q6svfak7.execute-api.ap-east-1.amazonaws.com/dev",
  COGNITO_ENDPOINTS: {
    authorization: "https://yt2p.auth.ap-southeast-1.amazoncognito.com/login",
    token: "https://yt2p.auth.ap-southeast-1.amazoncognito.com/oauth2/token",
    userInfo: "https://yt2p.auth.ap-southeast-1.amazoncognito.com/oauth2/userInfo",
    logout: "https://yt2p.auth.ap-southeast-1.amazoncognito.com/logout"
  }, 
  AUTH: {
    redirectUri: "https://yt2p-dev-webapp.josephw.work/login",
    logoutRedirectUri: "https://yt2p-dev-webapp.josephw.work/login",
    clientId: "60j7gsebnlt65q0kb6e1qr8cqg",
  },
  ROUTER: {
    base: '',
  }
};
