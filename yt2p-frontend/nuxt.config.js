import colors from "vuetify/es5/util/colors";


const CONFIG = (process.env.NODE_ENV !== 'production')?
               require("./config.local.js") :
                require("./config.production.js") ;
console.log(process.env.NODE_ENV, CONFIG);

export default {
  ssr: false,

  target: "static",

  head: {
    titleTemplate: "%s - a serverless Youtube to Podcast service",
    title: "YT2P",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },

  css: [],

  plugins: [],

  components: true,

  buildModules: ["@nuxtjs/vuetify"],

  modules: [
    "@nuxtjs/axios"
    , "@nuxtjs/auth-next"
  ],

  axios: {
    baseURL: CONFIG.AXIOS_BASE_URL,
  },

  auth: {
    strategies: {
      awsCognito: {
        scheme: "oauth2",
        endpoints: CONFIG.COGNITO_ENDPOINTS,
        token: {
          property: "access_token",
          type: "Bearer",
          maxAge: 3600
        },
        refreshToken: {
          property: "refresh_token",
          maxAge: 60 * 60 * 24 * 30
        },
        responseType: "token",
        redirectUri: CONFIG.AUTH.redirectUri,
        logoutRedirectUri: CONFIG.AUTH.logoutRedirectUri,
        clientId: CONFIG.AUTH.clientId,
        scope: ["email", "openid", "profile" , "yt2p-resource-server/api_user"],
        codeChallengeMethod: "S256"
      }
    }
  },

  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    theme: {
      dark:true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: "#42b883",
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  build: {},

  router: {
    base: CONFIG.ROUTER.base,
    middleware: ["auth"]
  }
};
