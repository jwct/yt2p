'use strict';
const Podcast = require("podcast");
const AWS = require("aws-sdk");
const s3 = new AWS.S3();
const docClient = new AWS.DynamoDB.DocumentClient();

const FEED_NAME = process.env.FEED_NAME;
const IMAGE_URL = process.env.FEED_IMG_URL;
const FEED_URL = 'http://'+process.env.S3_PODCASTHOST+'/podcast.xml';
const BUCKET_NAME = process.env.S3_PODCASTHOST;
const PODCAST_TABLE = process.env.DYNAMODB_PODCAST;

const feed = new Podcast({
  title: FEED_NAME
  , description: FEED_NAME
  , feed_url: FEED_URL
  , site_url: FEED_URL
  , image_url: IMAGE_URL
  , docs: 'http://'+BUCKET_NAME+'/podcast.xml'
  , categories: ['CATEGORY']
  , pubDate: Date.now()
  , itunesSubtitle: FEED_NAME
  , itunesSummary: FEED_NAME
  , ituneBucketsExplicit: false
  , itunesCategory: [
    { "text": "CATEGORY" }
  ]
});

module.exports.genPodcast = function () {
  var queryParam = {
    TableName: PODCAST_TABLE
    , ScanFilter: {
      "status": { "AttributeValueList": [{ "S": "HIDDEN" }], "ComparisonOperator": "NE" }
    }
  };

  docClient.scan(queryParam, function (err, data) {
    if (err) {
      console.log("error", err);
    } else {
      //      console.log("query ok ");
      data.Items.forEach((item) => {
        //        console.log(item.id.N,item.desc.S,item.url.S);
        
        feed.addItem({
          title: item.desc
          , url: item.url
          , guid: item.url
          , date: Date.now()
          , categories: ['podcast']
          , itunesExplicit: false
          , itunesSubtitle: item.desc
          , itunesSummary: item.desc
          , itunesDuration: 0
          , itunesKywords: ['podcast']
          , enclosure: { url: item.url }
        });
        
      });
      
      var xml = feed.buildXml();
      // console.log(xml);
      
      var bucketName = BUCKET_NAME;
      var params = { Bucket: bucketName, Key: 'podcast.xml', Body: xml };
      
      s3.putObject(params, function (err, data) {
        if (err)
          console.log(err);
      });
      
    }
  });
  return;
};
