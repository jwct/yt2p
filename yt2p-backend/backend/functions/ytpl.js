'use strict';
const ytpl = require('ytpl');
const async = require('async');
const AWS = require("aws-sdk");
const lambda = new AWS.Lambda();
const docClient = new AWS.DynamoDB.DocumentClient();

const {genPodcast} = require("../functions/genPodcast.js");

//////////////////////////////////////////
const s3Bucket = process.env.S3_PODCASTHOST;
const s3Folder = 'data/';
const playlistId = process.env.YT_PLAYLIST_ID;
const PODCAST_TABLE = process.env.DYNAMODB_PODCAST;
//////////////////////////////////////////
function insertDB(context, callback) {
  const playlist = context.out.playlist;
  var paramsItems = [];
  
  playlist.forEach(item => {
    paramsItems.push(
      {
        PutRequest: {
          Item: {
            id: item.id
            , createDate: `${new Date().toISOString()}` 
            , desc: item.title 
            , url: `http://${s3Bucket}/${s3Folder}${item.id}.m4a`
            , status: "NEW" 
          }
        }
      }
    );
  }
  );
  
  var params = {
    RequestItems: {
      [PODCAST_TABLE]: paramsItems
    }
  };
  console.log(JSON.stringify(params));
  
  docClient.batchWrite(params, function (err, data) {
    if (err) {
      callback('failed to upload dynamodb' + err, context);
    } else {
      callback(null, context); // success
    }
  });
}

function getPlaylist(context, callback) {
  Promise.resolve(ytpl(context.argv.playlistId))
         .then(
           (playlist) => {
             context.out.playlist = playlist.items;
             callback(null, context);
           }
         )
         .catch(err => callback('getPlaylist error:' + err, context));
}

function invokeYtdl(context, callback) {
  var queryParam = {
    TableName: PODCAST_TABLE
    , ScanFilter: {
      "status": { "AttributeValueList": [ "NEW" ], "ComparisonOperator": "EQ" }
    }
  };

  docClient.scan(queryParam, function (err, data) {
    if (err) {
      console.log("error", err);
    } else {
      data.Items.forEach((item) => {
        var id = item.id;
        console.log(`invoke ${id}`);
        var params = {
          FunctionName: process.env.LAMBDA_CONVERT, // the lambda function we are going to invoke
          InvocationType: 'Event',
          LogType: 'Tail',
          Payload: `{ "id" : "${id}" }`
        };
        
        lambda.invoke(params, function (err, data) {
          if (err) {
            console.log(err)
          } else {
            console.log(`ytdl : ${id} completed`);
          }
        });
      });
    }
  });
  callback(null, context);
}
function invokeGenPodcast(context, callback) {
  genPodcast();
  callback(null, context);
}
function printPlaylist(context, callback) {
  const playlist = context.out.playlist;
  playlist.forEach(
    (item) => console.log(item.title, item.id)
  );
  callback(null, context);
}

function onFinish(err, result) {
  console.log('finished');
  if (err) {
    console.log(err);
  }
  console.log(result);
}

function main() {
  async.waterfall(
    [
      // 0. init input arg
      (callback) => {
        var context = {
          argv: {
            "playlistId": playlistId
          },
          out: {"playlist": null},
          err : null
        };
        callback(null, context);
      }
      , getPlaylist
      //, printPlaylist
      , insertDB
      , invokeYtdl
      , invokeGenPodcast
    ]
    , onFinish
  );
}

module.exports.ytpl = function () {
  main();
}

