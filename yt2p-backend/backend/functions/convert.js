'use strict';

const AWS = require("aws-sdk");
const async = require('async');
const ytdl = require('ytdl-core');
const ffmpeg = require('fluent-ffmpeg');
const fs = require('fs');
const docClient = new AWS.DynamoDB.DocumentClient();

const tempDir = '\/tmp\/';
//const quality = [249, 140];
const quality = [140];
const s3Bucket = process.env.S3_PODCASTHOST;
const PODCAST_TABLE= process.env.DYNAMODB_PODCAST;
const s3Folder = 'data/';

function getYtdlInfo(context, callback) {
    ytdl.getInfo(context.argv.url
//        , { filter: 'lowestaudio' }
    ).then(
        info => {
            try {

                context.out.vid = info.videoDetails.videoId;
                context.out.format = ytdl.chooseFormat(info.formats, { quality: quality });
                context.out.title = info.videoDetails.title;

                callback(null, context);
            } catch (e) {
                context.err = e;
                callback('No format error', context);
            }
        })
        .catch(err => {
            context.err = err;
            callback('ytdl.getInfo', context);
        })
        ;

}

function doYtdl(context, callback) {
    let ytdlStream = ytdl(context.argv.url, { quality: context.out.format.itag });

    context.out.ytdlStream = ytdlStream;
    callback(null, context);
}

function doFfmpeg(context, callback) {
    try {
        context.out.resultFile = tempDir + context.out.vid + '.m4a';

        ffmpeg(context.out.ytdlStream)
            .audioBitrate(32)
            .audioChannels(1)
            .format('mp4')
            .save(context.out.resultFile)
            .on('error', (err, stdout, stderr) => {
                context.err = err;
                callback('doFfmpeg err', context);
            })
            .on('end', () => {
                callback(null, context);
            })
            ;
    } catch (e) {
        context.err = e;
        callback('doFfmpeg', context);
    }
}

function s3upload(context, callback) {
    const s3 = new AWS.S3();

    fs.readFile(context.out.resultFile, (err, data) => {
        if (err) {
            context.err = err;
            callback('s3upload err', context);
        }
        const params = {
            Bucket: s3Bucket, Key: s3Folder + context.out.vid + '.m4a', Body: data
        };

        s3.upload(params, function (s3Err, data) {
            if (s3Err) {
                context.err = s3Err;
                callback('s3upload err', context);
            }
            console.log(`File uploaded successfully at ${data.Location}`);
            callback(null, context);
        });
    });
}
function updateDB(context, callback) {
  var params = {
    TableName: PODCAST_TABLE
    ,Key: {
      "id": context.argv.id
    }
    ,UpdateExpression: "SET #status = :status"
    ,ExpressionAttributeNames : {"#status": "status"}
    ,ExpressionAttributeValues: {
      ":status": "DONE" 
    }
  };

  docClient.update(
    params
    , function (err, data) {
      if (err) {
        callback(err, context);
      } else {
        callback(null, context);
      }
    }
  );
}

function onDownloadFinish(err, context) {
  console.log('finished');
  if (err) {
    console.log(err);
    context.argv.globalCallback(err, 400);
  }
  console.log(context);
  context.argv.globalCallback(null, 200)
}


const main = function (globalCallback, id) {
  console.log(`convert ${id}`);
  async.waterfall(
    [
      (callback) => {
        var context = {
          argv: {
            "id": id
            , "url": "http://www.youtube.com/watch?v=" + id
            , "globalCallback": globalCallback
          }
          , out: {
            format: null         // JSON object from ytdl.info
            , vid: null          // youtube ID
            , resultFile: null   // full path of downloaded youtube m4a
            , ytdlStream: null   // data stream of ytdl
            , title: null        // youtube title
          }
          , err: null
        };
        callback(null, context);
      }
      , getYtdlInfo
      , doYtdl
      , doFfmpeg
      , s3upload
      , updateDB
    ]
    , onDownloadFinish
  );
};

function validateEvent(event) {
  if (!('id' in event)) {
    console.log('id not exists');
    return false;
  }
  return true;
}

module.exports.handler = function (event, context, callback) {
  console.log(`convert ${event}`);
  
  if (!validateEvent(event)) {
    callback('Validation Error', 400);
  } else {
    main(callback, event.id);
    callback(null, 200);
  }
}
