'use strict';


module.exports.getResponse = function getResponse(statusCode , result) {
  return {
    statusCode: statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify(result)
  };
}
