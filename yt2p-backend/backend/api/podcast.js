'use strict';

const PODCAST_TABLE = process.env.DYNAMODB_PODCAST;

const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient();
const {getResponse} = require("../util/common.js");

// ------------------------
// API : [baseURL]/podcasts
// method : put
// ------------------------
module.exports.putHandler = (event, context, callback) => {
  var body = JSON.parse(event.body);
  
  var params = {
    TableName: PODCAST_TABLE,
    Item:{
      "id": body.id ,
      "desc":body.desc ,
      "url":body.url ,
      "status":body.status,
      "createDate": `${new Date().toISOString()}` 
    }
  };

  docClient.put(params, function(err, data) {
    if (err) {
      console.error("Unable to create item. Error JSON:", JSON.stringify(err));
      callback(null, getResponse(500,'Error : ' + err));
    } else {
      console.log("Create succeeded:",params);
      callback(null, getResponse(200, params));
    }
  });
};

// ------------------------
// API : [baseURL]/podcasts/{param}
// method : post
// ------------------------
module.exports.postHandler = (event, context, callback) => {
  var param = event['pathParameters']['param'];
  var body = JSON.parse(event.body);
  
  var params = {
    TableName: PODCAST_TABLE,
    Key:{
      "id": param
    },
    UpdateExpression: "set #desc = :d, #url=:u, #status=:s",
    ExpressionAttributeNames:{
        "#desc": "desc",
        "#url":"url",
        "#status":"status"
    },
    ExpressionAttributeValues:{
      ":d": body.desc ,
      ":u": body.url ,
      ":s": body.status
    },
    ReturnValues:"UPDATED_NEW"
  };

  docClient.update(params, function(err, data) {
    if (err) {
      console.error("Unable to update item. Error JSON:", JSON.stringify(err));
      callback(null, getResponse(500,'Error : ' + err));
    } else {
      console.log("Update succeeded:",data);
      callback(null, getResponse(200, data));
    }
  });
};
// ------------------------
// API : [baseURL]/podcasts
// method : delete
// ------------------------
module.exports.deleteMultipleHandler = (event, context, callback) => {
  var body = JSON.parse(event.body);
  
  var paramsItems = [];
  
  body.id.forEach(item => {
    paramsItems.push(
      {
        DeleteRequest: {
          "Key": { 
            id: item
          }
        }
      }
    );
  }
  );
  
  var params = {
    RequestItems: {
      [PODCAST_TABLE]: paramsItems
    }
  };
  console.log(JSON.stringify(params));
  
  docClient.batchWrite(params, function (err, data) {
    if (err) {
      callback(null, getResponse(500, 'Error : ' + err));
    } else {
      console.log('delete completed');
      callback(null, getResponse(200, event.body));
    }
  });
}

// ------------------------
// API : [baseURL]/podcasts
// method : get
// ------------------------
module.exports.getAllHandler = (event, context, callback) => {
  var queryParam = {
    TableName: PODCAST_TABLE
  };
  
  docClient.scan(queryParam, function (err, data) {
    if (err) {
      console.log("error", err);
      callback(null, getResponse(500,'Error : ' + err));
    } else {
      var result = [];
      data.Items.forEach((item) => {
        result.push(item);
      });
      callback(null, getResponse(200,result));
    }
  });
};
