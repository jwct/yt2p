'use strict';

const s3Bucket = process.env.S3_PODCASTHOST;
const s3Folder = 'data/';

const AWS = require("aws-sdk");
const s3 = new AWS.S3();
const {getResponse} = require("../util/common.js");

// ------------------------
// API : [baseURL]/cleanS3
// method : post
// ------------------------
module.exports.postHandler = (event, context, callback) => {
  emptyBucket(callback);
};

function emptyBucket(callback){
  var params = {
    Bucket: s3Bucket,
    Prefix: s3Folder
  };

  s3.listObjects(params, function(err, data) {
    if (err) return callback(null, getResponse(400, err));

    if (data.Contents.length == 0) callback(null, getResponse(200, params));

    params = {Bucket: s3Bucket};
    params.Delete = {Objects:[]};
    
    data.Contents.forEach(function(content) {
      params.Delete.Objects.push({Key: content.Key});
    });

    s3.deleteObjects(params, function(err, data) {
      if (err) return callback(null, getResponse(400, err));
      if (data.IsTruncated) {
        emptyBucket(callback);
      } else {
        callback(null, getResponse(200, params));
      }
    });
  });
}

