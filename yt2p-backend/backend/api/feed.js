'use strict';

const {getResponse} = require("../util/common.js");
const {ytpl} = require("../functions/ytpl.js");

module.exports.postHandler = (event, context, callback) => {
  // copy playlist to dynamodb
  ytpl();

  callback(null,getResponse(200,'OK'));
}
