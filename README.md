# YT2P - a serverless Youtube to Podcast service
This application consist of three modules:

1. yt2p-layer : A Lambda layer of FFMPEG binary for audio encoding.
2. yt2p-backend : API for podcast creation.
3. yt2p-frontend : a Nuxt.js application for managing podcast feed.

## Setup

1. install serverless cli:
```
npm install serverless -g
```
2. In AWS IAM, create a new user with ```AdministratorAccess``` policy.
3. set AWS environment variable
```
export AWS_ACCESS_KEY_ID=[AWS Access Key] 
export AWS_SECRET_ACCESS_KEY=[AWS Secret Access Key]
```
4. Setup a Cognito user pool


## yt2p-layer

### Deployment
1. run ```makelayer.sh``` to download the latest ffmpeg binary
2. To deploy layer, run ```serverless deploy```
3. After deployed the layer, record down the latest ARN of FFMPEG layer


## yt2p-backend

### Setup
In serverless.yml, update the following attributes

* provider > enironment > YT_PLAYLIST_ID : your youtube playlist ID 
* provider > enironment > USER_POOL_ARN : your Cognito user pool ARN
* functions > convert > layers : ARN of FFMPEG layer

### Deployment

1. To deploy, run ```serverless deploy```
2. After the backend was deployed, record down the API endpoint url

## yt2p-layer

### Setup
Update backend API endpoints url, Cognito endpoints url , user pool client ID and redirect uri to ```config.local.js``` and ```config.production.js``` 

### Local test

run ```npm run dev``` to start in local mode

### Deployment

1. run ```npm run generate``` to generate the deployable
2. To deploy, run ```serverless deploy```
3. After deploy, 
	1. in CloudFront and route 53, setup DNS and SSL for the application.

Remark : ```sls deploy``` will auto invalidate CloudFront distribution. To manual invalid, you can use ```sls cloudfrontInvalidate```